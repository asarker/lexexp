"""

Script for automatically generating lexical variants from lexicons.

Please cite:

Sarker A. LexExp: A system for automatically expanding concept lexicons for noisy biomedical texts.
Bioinformatics. 2020 Nov 27;btaa995. doi: 10.1093/bioinformatics/btaa995. PMID: 33244602

Author: Abeed Sarker

"""

from collections import defaultdict
from gensim.models import KeyedVectors
from itertools import product
import Levenshtein, re
import numpy as np
from math import cos,pi
import nltk
import sys
from nltk.corpus import stopwords
STOPWORDS = stopwords.words('english')

'''
Decay Functions: determine how lexical similarity threshold changes with length of string.
Good initial values provided with the function.
'''

def linearDecay(token, IT=0.95, m=2, n=3, minim=0.4):
    '''
    Threshold decreases linearly with length.

    default: m = 2; n = 3
    '''
    NT = IT
    if len(token)>3:
        NT = IT - ((m*(len(token)-n))/100.0)
    return max(NT,minim)

def cosineDecay(token, IT=0.95, m=2, n=3, minim = 0.4):
    '''
    Threshold decreases with increasing gradient as length increases.
    default: m = 2; n = 3
    '''
    l = len(token)
    NT = min(max(cos(max(l-n,0)/(m*2*pi)),minim),IT)
    return NT

def exponentialDecay(token, m=4, n=0.4):
    '''
    Threshold decreases with decreasing gradient as length increases
    default: m = 4; n = 0.4
    '''
    IT = 0.95
    i = len(token)
    NT = min(i * m * np.exp(-.5*i) + n, IT)
    return NT


def backward_forward_bigrams(list_of_bigrams):
    '''
    Helper function.
    returns the unrepeated sets of bigrams
    '''
    unigram_list_forward = []
    unigram_list_backward = []
    curr = 1
    prev = 0
    unigram_list_forward.append(list_of_bigrams[prev])
    while curr < len(list_of_bigrams):
        if not list_of_bigrams[curr]== list_of_bigrams[prev] \
                and not (list_of_bigrams[curr] in list_of_bigrams[prev]):
            unigram_list_forward.append(list_of_bigrams[curr])
        prev=curr
        curr+=1
    curr = len(list_of_bigrams)-1
    prev = curr-1
    unigram_list_backward.append(list_of_bigrams[curr])
    while curr > 0:
        if not list_of_bigrams[curr]== list_of_bigrams[prev] \
                and not (list_of_bigrams[prev] in list_of_bigrams[curr]):
               unigram_list_backward.append(list_of_bigrams[prev])
        curr = prev
        prev-=1
    unigram_list_backward.reverse()
    return unigram_list_backward, unigram_list_forward

def remove_redundancy(original_ngram,list_of_terms):
    '''
    Helper function.
    removes redundant terms from the expanded text by comparing similarities with the original n-gram
    '''
    listtocompare = []
    clean_list = []
    items = original_ngram.split('_')
    for i in range(0,len(items)):
        listtocompare.append(items[i])
    for i in range(0, len(items)-1):
        listtocompare.append(items[i]+items[i+1])
    for lt in list_of_terms:
        for ltc in listtocompare:
            lvr = Levenshtein.ratio(str(lt), ltc)
            if lvr > 0.90:
                clean_list.append(lt)
                break
    return clean_list

def updateThreshold(substr, decay_function, initial_threshold):
    '''
        Updates threshold based on the decay function chosen.

        Decay functions:
            - s -> static
            - l -> linear (default)
            - c -> cosine
            - e -> exponential
    '''
    multiplication_factor = 1.1
    if decay_function == 's':
        updated_threshold = initial_threshold
    elif decay_function == 'c':
        updated_threshold = cosineDecay(substr, initial_threshold * multiplication_factor)
    elif decay_function == 'e':
        updated_threshold = exponentialDecay(substr, initial_threshold)
    else:
        updated_threshold = linearDecay(substr, initial_threshold * multiplication_factor)

    return updated_threshold

def expand_Lexicon(lex_entries, word_vectors, decay_function = 'l', semantic_search_length=500, levenshtein_threshold = 0.95):
    """

    :param lex_entries:             list of words for which spelling variants are to be generated
    :param word_vectors:            the word vector model
    :param semantic_search_length   the number of semantically closest elements to include


    For multi-word expressions (anything above bi-grams), the code splits into bigram sequences and finds the most
    similar entities for those.

    """
    print('Expanding the lexicon...')
    vars = defaultdict(list)
    for lex_entry in lex_entries:
        tokens_in_seedphrase = lex_entry.split()
        bigrms = list(nltk.bigrams(tokens_in_seedphrase))
        lex_entry = '_'.join(tokens_in_seedphrase)
        terms_to_expand = []
        terms_to_expand.append(lex_entry)
        all_expanded_terms = []

        #use separate settings based on the number of tokens.
        if len(tokens_in_seedphrase)>2:
            setting = 3
        else:
            setting = 1

        while len(terms_to_expand)>0:
            t = terms_to_expand.pop(0) #remove the item
            all_expanded_terms.append(t) #add it to already expanded list
            if setting >1:
                multiplication_factor = 1.1
                list_of_unigram_lists = []
                unigram_variants = defaultdict(list)
                for subn in tokens_in_seedphrase:
                    if not subn in STOPWORDS and (len(subn) > 3):
                        try:
                            similars = word_vectors.most_similar(subn, topn=semantic_search_length)
                            for similar in similars:
                                similar_term = similar[0]
                                #compute the ratio and update the threshold based on the decay function
                                sub_seq_score = Levenshtein.ratio(str(similar_term),subn)

                                '''
                                Decay functions:
                                - s -> static
                                - l -> linear (default)
                                - c -> cosine 
                                - e -> exponential
                                '''
                                updated_threshold = updateThreshold(subn, decay_function,levenshtein_threshold)
                                if sub_seq_score>updated_threshold:
                                    unigram_variants[subn].append(similar_term)

                        except KeyError:
                            unigram_variants[subn]=[subn]
                    else:

                       unigram_variants[subn]= [subn]
                    if len(unigram_variants[subn])==0:#i.e. no similar entity was found..
                        unigram_variants[subn] = [subn]
                for k,v in unigram_variants.items():
                    list_of_unigram_lists.append(list(set(v)))
                var_combos = list(product(*list_of_unigram_lists))
                for vc in var_combos:
                    vars[lex_entry].append('_'.join(vc))

            if setting > 2:
                multiplication_factor = 1.1
                list_of_bigram_lists = []

                bigram_variants = defaultdict(list)
                for subt in bigrms:
                    subtstr = '_'.join(subt)
                    try:
                        similars = word_vectors.most_similar(subtstr, topn=semantic_search_length)
                        for similar in similars:
                            similar_term = similar[0]
                            sub_seq_score = Levenshtein.ratio(str(similar_term), subtstr)

                            updated_threshold = updateThreshold(subtstr,decay_function,levenshtein_threshold)
                            if sub_seq_score > updated_threshold:
                                # find all the lexically and semantically similar bigrams
                                bigram_variants[subtstr].append(similar_term)

                    except KeyError:
                        bigram_variants[subtstr] = [subtstr]
                for subt in bigrms:
                    subtstr = '_'.join(subt)
                    bigram_variants[subtstr].append(subtstr)
                for k, v in bigram_variants.items():
                    list_of_bigram_lists.append(list(set(v)))

                var_combos = list(product(*list_of_bigram_lists))
                for vc in var_combos:
                    all_terms = '_'.join(vc)
                    back,forw = backward_forward_bigrams(all_terms.split('_'))
                    #remove all the redundancies
                    filtered_bigram = remove_redundancy(lex_entry,back)
                    filtered_bigram2 = remove_redundancy(lex_entry,forw)
                    vars[lex_entry].append('_'.join(filtered_bigram))
                    vars[lex_entry].append('_'.join(filtered_bigram2))
            else:
              try:
                similars = word_vectors.most_similar(t, topn=semantic_search_length)
                for similar in similars:
                    similar_term = similar[0]
                    seq_score = Levenshtein.ratio(str(similar_term),lex_entry)
                    if seq_score>updateThreshold(lex_entry,decay_function,levenshtein_threshold):#levenshtein_threshold:
                            vars[lex_entry].append(similar_term)
                            if not similar_term in all_expanded_terms and not similar_term in terms_to_expand:
                                terms_to_expand.append(similar_term)
              except KeyError:
                pass
        vars[lex_entry] = list(set(vars[lex_entry]))

    #completed generating the variants.
    for k,v in vars.items():
        vrep = []
        for e in v:
            e = re.sub(r'\W+', ' ', e)
            e = re.sub(r'[\_\W |\W\_]', '_', e)
            print (e)
            vrep.append(str.strip(e))
        vars[k] = list(set(vrep))
    print('===============')
    return vars

def pad(drugname, required_length):
    """
    Helper function for padding terms when computing weighted Levenshtein Ratio
    """
    post_padding = ''
    for i in range(0,required_length):
        if i<len(drugname):
            post_padding+=drugname[i]
        else:
            post_padding+='?'
    return post_padding



def loadVectorModel(filename):
    """
        Loads and returns a word2vec vector model
        :param filename: the filename for the model
        :return: the loaded model
    """
    print ('loading model this may take a while')
    word_vectors = KeyedVectors.load_word2vec_format(filename, binary=True, encoding='utf8', unicode_errors='ignore')
    return word_vectors

def loadEntriesFromLexicon(filename):
    """
        Given a filename that contains a set of words (line separated), loads and returns them..

    :param filename: Filename for the lexicon. Input should contain lexicon entries separated by newline.

    :return: a list of lexicon entries.
    """
    seedwordlist = []
    infile = open(filename)

    for line in infile:
        seedwordlist.append(str.strip(line).lower())
    print('Printing lexicon for test...')
    for l in seedwordlist:
        print (l)
    return seedwordlist

if __name__ =='__main__':
    '''
        need to specify:
            - lexicon name
            - path to word2vec vector
            - initial threshold
            - decay function to use 
            - output filename
    '''

    lexicon_entries = []
    vector_model_filename = ''
    output_filename = 'output' #default
    INITIAL_THRESHOLD = 0.95
    N_MOST_SIMILAR = 5000

    try:
        lexicon_path = sys.argv[1]
        vector_model_filename = sys.argv[2]

        try:
            INITIAL_THRESHOLD = float(sys.argv[3])
            decay_function = sys.argv[4]
            output_filename = sys.argv[5]
        except IndexError:
            print ('threshold/decay function/output not specified... using default values')
        infile = open(lexicon_path)
        for line in infile:
            item = str.strip(line)
            lexicon_entries.append(item)
        print('Loaded lexicon ... ')
        print('Loading the vector model ...')

        # LOAD THE VECTOR MODEL
        word_vectors = loadVectorModel(vector_model_filename)

    except IndexError:
        print ('Incorrect arguments specified (may be you didn\'t specify any arguments..')
        print ('At a minimum, the lexicon and the embedding model must be specified')
        print ('Format: python [scriptname] [path_to_lexicon] [path_to_word_vector_model]')


    #DEFAULT SETTING
    print('Generating the variants...')
    generated_lexicon_entry_variants = expand_Lexicon(lexicon_entries, word_vectors, semantic_search_length=N_MOST_SIMILAR, levenshtein_threshold=INITIAL_THRESHOLD)
    lexout = open(output_filename+'_'+str(INITIAL_THRESHOLD)+'.txt','w')
    for k,v in generated_lexicon_entry_variants.items():
        print (k, '\t', '||'.join(v))
        seedentry = k
        variants = '||'.join(v)
        seedentry  = re.sub('\_', ' ', seedentry)
        variants  = re.sub('\_', ' ', variants)
        lexout.write(seedentry + '$$$$' + variants + '\n')
    print ('----------')
    lexout.close()
print
print ('Done...')
   