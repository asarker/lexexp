# LexExp
A script for generating lexical variants from lexicons (from noisy sources such as EHRs and social media).

Algorithm and citation:
Sarker A. LexExp: A system for automatically expanding concept lexicons for noisy biomedical texts. Bioinformatics. 2020 Nov 28;btaa995.
PMID 33244602.

[LINK TO MANUSCRIPT](https://pubmed.ncbi.nlm.nih.gov/33244602/)


# Sample usage:

## Default settings
python lexexp.py lexicon.txt embeddingmodel.bin 

## Customized settings
python lexexp.py lexicon.txt embeddingmodel.bin 0.98 l outputfilename

## Additional resources
[Sample embedding model](https://data.mendeley.com/datasets/dwr4xn8kcv/3)

Sample lexicon is provided with the script (see: [lexicon.txt](https://bitbucket.org/asarker/lexexp/src/master/lexicon.txt))


## REQUIREMENTS

Please see requirements.txt for dependencies.

## Contact
abeed@dbmi.emory.edu
Note: If you are interested in using this tool for health-related research but lack the expertise to run it, feel free to contact our lab and we will strive to provide support.
